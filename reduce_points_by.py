# -*- coding: utf-8 -*-
"""
Copyright Counts.Pro
https://counts.pro
@author: Nathaniel Bass
nbass@counts.pro
git clone git@bitbucket.org:lighght/countspro.git@bitbucket
"""

import sys
import pandas as pd
from CountsPro import countsProData as cp

def reduce_points_by_input_command_line(number_points=4, infile=None, outfile=None):
    def errormsg():
        print("\n\
use syntax \"python countsProData.py [group size (number of points)] \
[input file.csv] [output file.csv]\" \nTo run with no parameters, \
you must have a file called \"python example.csv\" in the root directory"
            )
    try: number_points = number_points or int(sys.argv[1])
    except IndexError: pass
    except: errormsg()
    try: infile = infile or sys.argv[2]
    except IndexError:
        infile = "counts_pro_example.csv"
    except: errormsg()
    try: outfile = outfile or sys.argv[3]
    except IndexError:
        outfile = "{}_by_{}.csv".format(
            "".join(infile.split(".")[:-1]), 
            number_points
            )
    except: errormsg()
    return number_points, infile, outfile

def main():
    number_points, infile, outfile = reduce_points_by_input_command_line()
    dfmeta, df = cp.read_csv_data(infile)
    df = cp.reduce_points_by(dfmeta, df, number_points)
    cp.write_csv_data(dfmeta, df, outfile)

if __name__ == '__main__':
    main()
