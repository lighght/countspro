# -*- coding: utf-8 -*-
"""
Copyright Counts.Pro
https://counts.pro
@author: Nathaniel Bass
nbass@counts.pro
git clone git@bitbucket.org:lighght/countspro.git
"""

import simplekml

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import colors
from matplotlib import cm

from CountsPro import countsProData as cp

def summary_plot(df, cmap=None, plot_interval=1):
    cmap = cmap or cm.get_cmap('viridis')
    fig, ax = plt.subplots(4, 1, figsize=(5, 15), tight_layout=True)
    vlognorm = [
        df['count_a'][df['count_a']>0].min(),
        df['count_a'].max(),
        ]
    vlognorm = colors.LogNorm(*vlognorm)
    
    ax[0].scatter(
        x = df['longitude'], 
        y = df['latitude'],
        # weights = df['count_a'],
        c = df['count_a'],
        # c = df['cpm_ema_a'],
        cmap = cmap,
        norm = vlognorm,
        zorder = 1,
        )
    ax[0].plot(
        df['longitude'], 
        df['latitude'],
        linewidth = 1,
        color = '#00000033',
        zorder = 0,
        )
    ax[1].hist(
        df['count_a']*4*60,
        bins = np.max(df['count_a'])-np.min(df['count_a'])
        )
    ax[2].scatter(
        x = df['datetime'][::plot_interval], 
        y = df['latitude'][::plot_interval],
        marker='+',
        color = 'm',
        )
    ax[2].twinx().scatter(
        x = df['datetime'][::plot_interval], 
        y = df['longitude'][::plot_interval],
        marker='o',
        color = 'b',
        )
    ax[2].twinx().scatter(
        x = df['datetime'][::plot_interval], 
        y = df['location_accuracy'][::plot_interval],
        marker='o',
        color = 'y',
        )
    ax[3].plot(
        pd.to_datetime(df['datetime']),
        df['count_a']*4*60,
        color=[0,0,1,0.5],
        )
    ax[3].plot(
        df['datetime'],
        df['cpm_ema_a'],
        color=[1,0.2,1,0.9],
        )
    plt.show()
    return

def counts_pro_kml_pnt(df, kml,):
    pnt = kml.newpoint(
        coords=[(df['latitude'],df['longitude'])],
        description="Channel A: {} counts".format(df['count_a']),
        timestamp=simplekml.TimeStamp(df['datetime']),
        )
    pnt.style = simplekml.Style(
        iconstyle=simplekml.IconStyle(
            color=df['color'],
            icon=simplekml.Icon(href='http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png'),
            scale=0.5,
            hotspot=simplekml.HotSpot(
                x=0.5,y=0.5,xunits='fraction',yunits='fraction',
                ),
            ),
        )
    return df

def main():
    infile = "counts_pro_example.csv"
    outfile = "counts_pro_example.kml"
    
    dfmeta, df = cp.read_csv_data(infile)
    
    print("Max value of count_a, 250 ms= {} ".format(np.max(df['count_a'])))
    
    summary_plot(df,)
    
    kml = simplekml.Kml()

    color_norm = colors.Normalize(vmin=np.min(df['count_a']),vmax=np.max(df['count_a']))
    color_norm = cm.ScalarMappable(norm=color_norm, cmap=cm.viridis)
    
    df['color'] = df.apply(lambda x: colors.to_hex(color_norm.to_rgba(np.asarray(x['count_a'])),keep_alpha=True), axis=1)
    df['color'] = df.apply(lambda x: "".join(np.asarray([*x['color']])[[0,7,8,5,6,3,4,1,2]]), axis=1)
    
    df.apply(lambda x: counts_pro_kml_pnt(x, kml,), axis=1)
    
    kml.save(outfile)
    print(kml.kml()[:1000])
    
    return

if __name__ == '__main__':
    main()

