# -*- coding: utf-8 -*-
"""
Copyright Counts.Pro
https://counts.pro
@author: Nathaniel Bass
nbass@counts.pro
git clone git@bitbucket.org:lighght/countspro.git
"""

import pandas as pd
from dateutil import parser

counts_pro_meta_dtype = {
    'name': 'string',
    'description': 'string',
    'channel_a': 'string',
    'channel_b': 'string',
    'notes': 'string',
    'device_name': 'string',
    'device_serial': 'string',
    'probe_name': 'string',
    'probe_serial': 'string',
    'started_at': 'string',
    'ended_at': 'string',
    'duration_seconds': 'f8',
    'total_counts_a': 'u8',
    'total_counts_b': 'u8',
    }

counts_pro_data_dtype = {
    'datetime': 'string',
    'position' : 'u8',
    'count_a': 'u8',
    'cpm_ema_a': 'u8',
    'count_b': 'u8',
    'cpm_ema_b': 'u8',
    'latitude': 'f8',
    'longitude': 'f8',
    'altitude': 'f8',
    'location_accuracy': 'f8',
    'device_time': 'u8',
    }

def read_csv_data(infile, quiet=False):
    dfmeta = pd.read_csv(
        infile,
        nrows=1,
        header=0,
        dtype=counts_pro_meta_dtype,
        )
    df = pd.read_csv(
        infile,
        header=2,
        dtype=counts_pro_data_dtype,
        )
    dfmeta['started_at'] = pd.to_datetime(dfmeta['started_at'])
    dfmeta['ended_at'] = pd.to_datetime(dfmeta['ended_at'])
    df['datetime'] = pd.to_datetime(df['datetime'])
    if quiet == False:
        print("Input File: \"{}\"".format(infile))
    return dfmeta, df

def write_csv_data(dfmeta, df, outfile=None, quiet=False):
    outfile=outfile or "write_csv_data.csv"
    dfmeta_ = dfmeta.copy()
    df_ = df.copy()
    
    # The following bit with the replacement is to match the original
    #   Counts Pro data. 
    dfmeta_['started_at'] = dfmeta_['started_at'].astype('string')
    dfmeta_['ended_at'] = dfmeta_['ended_at'].astype('string')
    df_['datetime'] = df_['datetime'].astype('string')
    
    dfmeta_.replace(regex=r'([0-9- :.]+)000\+00:00',value="\g<1>Z",inplace=True)
    df_.replace(regex=r'([0-9- :.]+)000\+00:00',value="\g<1>Z",inplace=True)
    
    dfmeta_.to_csv(
        outfile,
        index=False,
        na_rep="",
        )
    with open(outfile, 'a') as f: f.write("\n")
    df_.to_csv(
        outfile,
        mode='a',
        index=False,
        )
    if quiet == False:
        print("Output File: \"{}\"".format(outfile))

def reduce_points_by(dfmeta, df, number_points=4, quiet=False):
    """
    Parameters
    ----------
    dfmeta : pd.Dataframe 
        dtype of counts_pro_meta_dtype
    df : pd.Dataframe 
        dtype of counts_pro_data_dtype
    number_points : Integer, default is 4.
        raw data is 250ms intervals. Group data for longer intervals
    outfile : string, The default is None.
    
    """
    df['count_a'] = (
        df.assign(red_points=lambda x : (x['position'] - 1) // number_points + 1)
        .groupby('red_points')['count_a'].transform(lambda x: x.sum())
        )
    if quiet==False:
        print("Points reduced by {}.".format(number_points))
    return df[number_points-1::number_points]

def dead_time_first_order(M, d, cal_const=1,):
    """N = M * (1 + d M) 
    
    Bécares and Blázquez - 2012 - Detector Dead Time Determination and Optimal Count
    
    N = count rate (corrected), usually counts per minute
    M = count rate (observed) 
    d = dead time (make such that units of the 'whatever' part of 'counts per whatever'. Minutes, probably)
    
    Returns
    -------
    R = N / cal_const = corrected count rate
    """
    N = M.copy()
    N *= d
    N += 1
    N *= M
    R = N / cal_const
    return R

def dead_time_non_paralyzable(M, d, cal_const=1,):
    """N = M / (1 - d M) 
    
    """
    N = M.copy()
    N *= d
    N -= 1
    N *= -1
    N **= -1
    N *= M
    R = N / cal_const
    return R

def dead_time_paralyzable(R, d, cal_const=1,):
    """N = N'exp(-N'd)
    
    """
    M = R.copy()
    M *= cal_const
    M *= d
    M *= -1
    M = np.exp(M)
    M *= R
    return M

def dead_time_linear(M, b=0, cal_const=1,):
    R = M.copy()
    R /= cal_const
    R += b
    return R


def main():
    return

if __name__ == '__main__':
    main()
